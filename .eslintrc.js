module.exports = {
  root: true,
  extends: '@react-native-community',
  globals: {
    jest: true
  },
  overrides: [
    {
      files: ['*.js', '*.jsx'],
      parser: 'babel-eslint',
      plugins: ['flowtype'],
      rules: {
        // Flow Plugin
        // The following rules are made available via `eslint-plugin-flowtype`
        // https://github.com/gajus/eslint-plugin-flowtype

        'flowtype/define-flow-type': 1,
        'flowtype/use-flow-type': 1,
      },
    }
  ],
  parserOptions: {
    ecmaFeatures: {
      legacyDecorators: true
    }
  }
};
