import React from 'react';
import { StyleSheet, Image, StyleProp, TouchableOpacity } from 'react-native';
import { useDimensions } from '@react-native-community/hooks';

import { Touchable } from 'components/Button';
import RouterService from 'services/RouterService';
import useObjectState from 'utils/hooks/useObjectState';
import stubObject from 'utils/stubObject';
import { isObject, isString } from 'lodash';

import type { ImageProps, ImageStyle } from 'react-native';

const RESOURCE_CONFIG = {
  INITIAL_STATE: {
    height: 0,
    interactionReady: false,
  },
};

type AdWidgetPropTypes = {
  url?: string,
  image: string | number,
  imageProps?: ImageProps,
  style?: StyleProp<ImageStyle>,
};
export default function AdWidget(props: AdWidgetPropTypes = {}) {
  const { window } = useDimensions();
  const [state, setState] = useObjectState(RESOURCE_CONFIG.INITIAL_STATE);

  const imageSource = normalizedImageSource(props.image);
  const imageStyle = StyleSheet.flatten([
    { width: window.width, height: state.height },
    props.style,
  ]);

  React.useEffect(() => {
    Image.getSize(props.image, (width, height) => {
      console.log('AdWidget.width', window.width, width, height, Math.ceil((height / width) * window.width));

      setState({
        interactionReady: true,
        height: Math.ceil((height / width) * window.width), // By this, you keep the image ratio
      });
    });
  }, [window.width]);

  return (
    state.interactionReady && (
      <Touchable
        activeOpacity={0.9}
        onPress={() => RouterService.showWebViewModal({ uri: props.url })}>
        <Image
          style={imageStyle}
          source={imageSource}
          resizeMode="contain"
          {...stubObject(props.imageProps)}
        />
      </Touchable>
    )
  );
}

/**
 *
 * @param source
 * @returns {{uri: *}|*}
 */
function normalizedImageSource(source) {
  if (isObject(source)) {
    return isString(source.uri) ? source : normalizedImageSource(source.uri);
  } else if (isString(source)) {
    return { uri: source };
  }

  return source;
}
