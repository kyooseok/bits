import React from 'react';
import { Linking, StyleSheet } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { WebView as RNWebView, WebViewProps } from 'react-native-webview';

import { invoke, isFunction } from 'lodash';
import { WEB_VIEW_SCREEN } from 'app/routeKeys';
import RouterService from '../services/RouterService';

export default function WebView(props: WebViewProps = {}) {
  const [isLoaded, setLoaded] = React.useState(false),
    { componentId, style, ...otherProps } = props;

  const onLoadProgress = syntheticEvent => {
    console.log('onLoadProgress', syntheticEvent.nativeEvent);
  };

  /**
   *
   * @param navigationState
   */
  const onNavigationStateChange = navigationState => {
    setLoaded(navigationState.loading === false);

    console.log('onNavigationStateChange', navigationState);
    invoke(props, 'onNavigationStateChange', navigationState);
  };

  /**
   *
   * @type {function(*): boolean}
   */
  const onShouldStartLoadWithRequest = evt => {
    // 사용자가 선언한 경우
    if (isFunction(props.onShouldStartLoadWithRequest)) {
      return invoke(props, 'onShouldStartLoadWithRequest', evt);
    }

    // 사용자가 요청한 링크와 현재 로딩 중인 링크가 동일할 경우
    if (evt.url === props.source.uri) {
      return true;
    }

    if (isLoaded) {
      setImmediate(() => {
        return RouterService.showWebViewModal({
          uri: evt.url,
          title: evt.title,
        });
      });

      return false;
    }

    // 아무런 액션을 하지 않음
    return true;
  };

  return React.cloneElement(
    <RNWebView
      originWhitelist={['*']}
      onNavigationStateChange={onNavigationStateChange}
      onShouldStartLoadWithRequest={onShouldStartLoadWithRequest}
      onLoadProgress={onLoadProgress}
      automaticallyAdjustContentInsets={false}
      style={[styles.container, style]}
    />,
    otherProps,
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
