/**
 * @author Martin, Lee
 *
 * @flow
 */
import React from 'react';
import {
  View,
  TouchableNativeFeedback,
  TouchableOpacity,
  TouchableOpacityProps,
  BackgroundPropType,
  Platform,
} from 'react-native';

const LOLLIPOP = 21;

export type TouchablePropTypes = TouchableOpacityProps & {
  background?: BackgroundPropType,
};

function ButtonComponent(props: TouchablePropTypes, ref) {
  const {
    background,
    useForeground,
    activeOpacity,
    style,
    children,
    ...otherProps
  } = props;

  /**
   *
   * Android 21 이상 일 경우 TouchableNativeFeedback 컴포넌트를 사용
   */
  if (Platform.OS === 'android' && Platform.Version >= LOLLIPOP) {
    const buttonProps = {
      ref,
      background,
      ...otherProps,
    };

    return (
      <TouchableNativeFeedback {...buttonProps}>
        <View style={style}>{children}</View>
      </TouchableNativeFeedback>
    );
  }

  const buttonProps = {
    ref,
    style,
    activeOpacity,
    ...otherProps,
  };

  return <TouchableOpacity {...buttonProps}>{children}</TouchableOpacity>;
}

export default React.forwardRef(ButtonComponent);
