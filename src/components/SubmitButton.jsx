import React from 'react';
import { StyleSheet, ActivityIndicator, View, Text, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';
import isObject from 'lodash/isObject';

import type { ButtonProps, ViewStyle } from 'react-native';

function stubObject(origin=null, placeholder={}) {
  if( isObject(origin) ) return origin;
  return placeholder;
}

const SUBMIT_HEIGHT = 48;
const SUBMIT_BACKGROUND_COLOR = '#043362';
const SUBMIT_TEXT_COLOR = '#FFFFFF';
const SUBMIT_DISABLED_BACKGROUND_COLOR = '#F2F2F2';
const SUBMIT_DISABLED_TEXT_COLOR = '#AEB2AE';

const BOX_SHADOW_STYLE = {
  shadowColor: "#333333",
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowOpacity: 0.20,
  shadowRadius: 1.41,

  elevation: 2,
};

const Container = styled(View)`
  border-radius: 6px;
  justify-content: center;
  align-items: center;
`;

const StyledText = styled(Text)`
  font-size: 16px;
  color: ${(props) => props.color || SUBMIT_TEXT_COLOR};
`;


/**
 *
 * @name Submit
 * @property {string} title
 * @property {number} height
 * @property {boolean} disabled
 * @property {boolean} submitting
 * @property {Function} handleSubmit
 * @property {ViewStyle} containerStyle
 */
type SubmitPropTypes = {
  title: string,
  height?: number,
  submitting?: boolean,
  handleSubmit(): void,
  containerStyle?: ViewStyle,
  buttonProps: ButtonProps,
  textProps: any,
}
const Submit = React.memo((props: SubmitPropTypes={}) => {
  const buttonProps = stubObject(props.buttonProps);

  // submitting 일 경우에도 버튼을 비활성화 시킨다.
  if( !!buttonProps.disabled === false ) {
    buttonProps.disabled = (props.submitting === true);
  }

  const color = buttonProps.disabled? SUBMIT_DISABLED_TEXT_COLOR: SUBMIT_TEXT_COLOR
    , backgroundColor = buttonProps.disabled? SUBMIT_DISABLED_BACKGROUND_COLOR: SUBMIT_BACKGROUND_COLOR
    , containerStyle = StyleSheet.flatten([BOX_SHADOW_STYLE, {height: props.height || SUBMIT_HEIGHT, backgroundColor}, props.containerStyle])
    , textProps = stubObject(props.textProps);

  const ButtonText =
    props.submitting
      ? <ActivityIndicator color="gray" />
      : <StyledText color={color} {...textProps}>{props.title}</StyledText>;

  return (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={props.handleSubmit}
      {...props.buttonProps}
    >
      <Container style={containerStyle}>
        {ButtonText}
      </Container>
    </TouchableOpacity>
  )
});

export default Submit;
