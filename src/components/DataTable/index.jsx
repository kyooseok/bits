import DataTable from './DataTable';
import DataTableRow from './DataTableRow';
import DataTableCell from './DataTableCell';

DataTable.Row = DataTableRow;
DataTable.Cell = DataTableCell;

export default DataTable;
export { DataTableRow, DataTableCell };
