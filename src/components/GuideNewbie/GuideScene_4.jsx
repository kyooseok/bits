// @flow
import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {Navigation} from 'react-native-navigation';

import Button from 'components/Button';
import AppCache, {ASK_NEWBIE_GUIDE_SAW} from 'app/cache';

import './styles.styl';
import RouterService from "../../services/RouterService";

type GuideSceneProps = {
  primary: string;
  secondary?: string;
}

export default function GuideScene_4({primary, secondary}: GuideSceneProps) {
  const handleSubmit = () => {
    AppCache.set(ASK_NEWBIE_GUIDE_SAW, true);
    return Navigation.dismissAllModals();
  };

  return (
    <View styleName="GuideScene">
      <View styleName="GuideSceneContainer">
        <View styleName="GuideScenePrimary">
          <Text styleName="GuideScenePrimaryText">{primary}</Text>
        </View>
        <View styleName="compacted">
          <View style={[StyleSheet.absoluteFillObject, {overflow: 'hidden'}]}>
            <Image source={require('./guide-scene-4.png')} resizeMode="cover" style={{alignSelf: 'center'}} />
          </View>
        </View>
        <View styleName="GuideSceneBottom">
          <View styleName="GuideSceneSecondary pv10">
            <Text styleName="GuideSceneSecondaryText">{secondary}</Text>
          </View>
          <View styleName="center">
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => handleSubmit()}
              styleName="GuideSceneButton bg-primary mt15">
              <Text styleName="text16 medium white">시작하기</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  )
}
