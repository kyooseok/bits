// @flow
import React from 'react';
import {View} from 'react-native';
import ViewPager  from '@react-native-community/viewpager';
import {times} from 'lodash';

import Circle from './Circle';
import GuideScene_1 from './GuideScene_1';
import GuideScene_2 from './GuideScene_2';
import GuideScene_3 from './GuideScene_3';
import GuideScene_4 from './GuideScene_4';

import './styles.styl';

export default function GuideNewbie(props) {
  const [position, setPosition] = React.useState(0);

  return (
    <View styleName="compacted">
      <ViewPager styleName="compacted" onPageScroll={(e) => setPosition(e.nativeEvent.position)}>
        <GuideScene_1 primary="국내 최초" secondary="주식 카피트레이딩 플랫폼" />
        <GuideScene_2 primary="실계좌 실시간 공개" secondary="모든 계좌 데이터는 실시간으로 공개" />
        <GuideScene_3 primary="간편한 계좌조회" secondary={`최초 1회 로그인으로\n 재로그인 없는 편리한 계좌조회`} />
        <GuideScene_4 primary="더한 CTS" secondary={`그 동안 경험하지 못했던\n혁신 핀테크 서비스`} />
      </ViewPager>

      <View styleName="Pagination">
        {times(4).map((index) => <Circle key={String(index)} active={index === position} style={{marginHorizontal: 6,}} />)}
      </View>
    </View>
  )
}
