// @flow;
import React from 'react';
import {View} from 'react-native';

export default function Circle({active, style, ...otherProps} = {}) {
  let backgroundColor = active? '#043361': '#b7b7b7';

  return (
    <View style={[{width: 6, height: 6, borderRadius: 7, backgroundColor}, style]} {...otherProps} />
  )
}
