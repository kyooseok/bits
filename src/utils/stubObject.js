import { isObject } from 'lodash';

export default function stubObject(origin=null, placeholder={}) {
  if( isObject(origin) ) return origin;
  return placeholder;
}
