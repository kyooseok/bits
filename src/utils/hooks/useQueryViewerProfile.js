import { useQuery } from 'react-query';
import { ProfileService } from 'services/apis/endpoints/accounts';

const RESOURCE_CONFIG = {
  QUERY_KEY: 'VIEWER_PROFILE_QUERY',
};

function queryFn(queryKey, ...queryVariables) {
  return ProfileService.getAsync()
    .then(response => {
      return {
        username: response.name,
        // 판매법인
        sales_company: response.role020,
        // 연동 상품
        trading_manager: response.agreementProduct,
        // 연동 비
        trading_ratio: response.fabotk_ratio,
      };
    })
    .catch(e => {
      console.warn(e);

      return {
        username: null,
        sales_company: null,
        trading_manager: '로그인 후 확인가능합니다.',
        trading_ratio: 0,
      };
    });
}

export default function useQueryViewerProfile(queryConfig = {}) {
  return useQuery(RESOURCE_CONFIG.QUERY_KEY, queryFn, queryConfig);
}
