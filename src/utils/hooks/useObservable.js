// @flow
import React from 'react';
import EnvStore from 'stores/env';
import SessionStore from 'stores/session';

export const ObservableContext = React.createContext({
  env: EnvStore,
  session: SessionStore,
});

export default function useObservable() {
  return React.useContext(ObservableContext);
}
