// @flow
import { useQuery } from 'react-query';
import { RankingService } from 'services/apis/endpoints/managers';

const RESOURCE_CONFIG = {
  QUERY_KEY: 'MANAGER_RANKING_QUERY',
};

function queryFn(queryKey, queryVariables) {
  const option = {
    params: {
      type: queryVariables?.category,
    },
  };

  return RankingService.queryAsync(option);
}

export default function useQueryManagers(category, queryConfig = {}) {
  return useQuery([RESOURCE_CONFIG.QUERY_KEY, { category }], queryFn, queryConfig);
}
