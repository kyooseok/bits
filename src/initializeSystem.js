/**
 *
 * 1. 캐시불러오기
 * 2. 패키지 초기화
 * 2. 자동로그인 동작
 * 3. 네비게이션 레이아웃 보여주기
 */

import { Navigation } from 'react-native-navigation';

import AppCache from './app/cache';
import configureStore from './stores/configureStore';
import registerScreen from './app/registerScreen';


Navigation.events().registerAppLaunchedListener(() => {
  AppCache.synchronize()
      // 앱 설정 시작
    .then(() => configureStore())
    .catch(e => {
      // TODO 네트워크가 유실되거나 다른 경우 앱을 시작못하는 케이스를 수집
      console.warn(e);
    })
    .then(() => registerScreen()) //화면등록
    .then(() => import('app/setup').then(app => app.setup()));
});
