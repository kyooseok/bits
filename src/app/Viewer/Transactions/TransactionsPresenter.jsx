/**
 * @flow
 */
import React from 'react';
import { View, Text, StyleProp, ViewStyle } from 'react-native';

import dayjs from 'dayjs';
import DataTable from 'components/DataTable';
import ChromaText from 'components/ChromaText';
import RatioText from 'components/RatioText';
import useObservable from 'utils/hooks/useObservable';

import './styles.styl';

export type ItemPropTypes = {
  uid: string,
  date: string,
  register_date: number,
  endDate: number,
  stockcode: string,
  ratio: number, // 수익률
  realized_profit_and_loss: number, // 실현손익
  purchase_amount: number, // 매입금액
  sell_amount: number, // 매도손익
  balance_amount: number, // 수량
};

export const ItemRow = React.memo(({ item }: { item: ItemPropTypes }) => {
  const { env } = useObservable();

  return (
    <View styleName="ItemRow">
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'baseline',
          paddingBottom: 5,
        }}>
        <View styleName="ItemRowTitle">
          <Text styleName="ItemRowTitleText">
            {env.getStockItem(item.stockcode)?.name}
          </Text>
        </View>
      </View>

      <View styleName="mb3">
        <Text styleName="text13 medium caption">
          <Text>보유기간 : </Text>
          <Text>{dayjs(item.register_date).format('YYYY.MM.DD')}</Text>
          <Text> ~ </Text>
          <Text>{dayjs(item.endDate).format('YYYY.MM.DD')}</Text>
        </Text>
      </View>

      <Text styleName="text13 medium caption">
        <Text>수익률 : </Text>
        <ChromaText value={item.ratio}>
          {Number.percentage(item.ratio)}
        </ChromaText>
        <Text>{'   '}</Text>
        <Text>수익금 : </Text>
        <ChromaText value={item.realized_profit_and_loss}>
          {Number.currency(item.realized_profit_and_loss)}
        </ChromaText>
      </Text>
    </View>
  );
});


export type CardViewPropTypes = {
  uid: string,
  date: string,
  register_date: number,
  endDate: number,
  stockcode: string,
  ratio: number, // 수익률
  realized_profit_and_loss: number, // 실현손익
  purchase_amount: number, // 매입금액
  sell_amount: number,
  balance_amount: number,
};

export const CardView = React.memo(({ item }: { item: CardViewPropTypes }) => {
  const { env } = useObservable();

  return (
    <View styleName="ItemRow">
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'baseline',
          paddingBottom: 5,
        }}>
        <View styleName="ItemRowTitle">
          <Text styleName="ItemRowTitleText">
            {env.getStockItem(item.stockcode)?.name}
          </Text>
        </View>
        <View styleName="ItemRowSubtitle">
          <Text styleName="ItemRowSubtitleText">{item.stockcode}</Text>
        </View>
      </View>

      <DataTable styleName="ItemRowDataTable">
        <DataTable.Row>
          <DataTableLabel label="평가금액" />
          <DataTableLabel label="수익률" />
          <DataTableLabel label="평가손익" />
        </DataTable.Row>
        <DataTable.Row>
          <DataTableValue value={item.valuation_amount} />
          <DataTableValue as={RatioText} value={item.interest_rate} />
          <DataTableValue as={ChromaText} value={item.valuation_gain_loss} />
        </DataTable.Row>

        <DataTable.Row>
          <DataTableLabel label="매입금액" />
          <DataTableLabel label="보유수량" />
          <DataTableLabel label="평균단가" />
        </DataTable.Row>
        <DataTable.Row>
          <DataTableValue value={item.purchase_amount} />
          <DataTableValue value={item.balance_amount} />
          <DataTableValue value={item.average_price} />
        </DataTable.Row>
      </DataTable>
    </View>
  );
});

type DataTableLabelPropTypes = {
  label: string,
};
function DataTableLabel(props: DataTableLabelPropTypes = {}) {
  return (
    <DataTable.Cell styleName="DataTableLabel">
      <Text styleName="DataTableLabelText">{props.label}</Text>
    </DataTable.Cell>
  );
}

type DataTableValuePropTypes = {
  as?: any,
  style?: StyleProp<ViewStyle>,
  value: string,
  currency?: string,
  formatter?: (value: number) => string,
};
function DataTableValue(props: DataTableValuePropTypes = {}) {
  const TextComponent = props.as ? props.as : Text;
  const formatter =
    typeof props.formatter === 'function'
      ? props.formatter
      : v => Number.currency(v, '');

  return (
    <DataTable.Cell numeric={true} styleName="DataTableValue">
      <TextComponent styleName="DataTableValueText" value={props.value}>
        {formatter(props.value)}
      </TextComponent>
    </DataTable.Cell>
  );
}

