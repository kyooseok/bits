/**
 *
 * @flow
 */
import React from 'react';
import { View, FlatList, Text } from 'react-native';
import { queryCache, useInfiniteQuery } from 'react-query';
import { useInteractionManager } from '@react-native-community/hooks';

import { TransactionService } from 'services/apis/endpoints/accounts';
import { ItemRow } from './TransactionsPresenter';

import './styles.styl';

const RESOURCE_CONFIG = {
  QUERY_KEY: 'VIEWER_TRANSACTIONS_QUERY_KEY',
};

function queryFn(queryKey: string, nextPage: ?number) {
  return TransactionService.queryAsync({
    params: {
      page_index: nextPage || 1,
      per_page: 10,
    },
  }).then(
    ({ current_item_count, total_items, page_index, total_pages, items }) => {
      return {
        current_item_count: parseInt(current_item_count, 10) || 0,
        total_items: parseInt(total_items, 10) || 0,
        page_index: parseInt(page_index, 10) || 0,
        total_pages: parseInt(total_pages, 10) || 0,
        items: Array.isArray(items) ? items : [],
      };
    },
  );
}

function ViewerTransactions(props = {}) {
  const interactionReady = useInteractionManager();

  const {
    status,
    data,
    fetchMore,
    canFetchMore,
    isFetchingMore,
  } = useInfiniteQuery(RESOURCE_CONFIG.QUERY_KEY, queryFn, {
    retry: false,
    staleTime: 1,
    getFetchMore({ page_index, total_pages }) {
      const nextPage = page_index + 1;
      return nextPage > total_pages ? false : nextPage;
    },
  });

  React.useEffect(() => {
    return () => {
      const targetQueryHash = queryCache.getQuery(RESOURCE_CONFIG.QUERY_KEY)
        .queryHash;

      return queryCache.refetchQueries(query => {
        if (query.queryHash === targetQueryHash) {
          query.pageVariables = [query.pageVariables[0]];
          return true;
        }
        return false;
      });
    };
  }, []);

  const onEndReached = () => {
    const executable =
      interactionReady &&
      status === 'success' &&
      (canFetchMore && isFetchingMore === false);

    return executable && fetchMore();
  };

  return (
    interactionReady && (
      <FlatList
        data={Array.isArray(data) ? data : []}
        initialNumToRender={10}
        keyExtractor={item => item.page_code}
        showsVerticalScrollIndicator={false}
        renderItem={({ item, index }) => (
          <React.Fragment key={String(index)}>
            {item.items.map(v => (
              <ItemRow item={v} />
            ))}
          </React.Fragment>
        )}
        onEndReachedThreshold={0.1}
        onEndReached={onEndReached}
        ListHeaderComponent={<View styleName="p6" />}
        ListEmptyComponent={
          status === 'success' && (
            <View
              style={{
                minHeight: 350,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text styleName="text14 regular caption">
                보유 중인 종목이 없습니다.
              </Text>
            </View>
          )
        }
        ListFooterComponent={
          <View styleName="p35 pb50 bg-white">
            <Text styleName="text13 regular caption text-align-center">
              증권사 수수료와 거래세가 공제되지 않았으며{'\n'}
              실제 손익과 일부 차이가 있을 수 있습니다.
            </Text>
          </View>
        }
        {...props}
      />
    )
  );
}

export default React.memo(ViewerTransactions);
