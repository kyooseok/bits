// @flow
import React from 'react';
import {
  Text,
  StyleProp,
  ViewStyle,
  View,
  FlatList,
  FlatListProps,
} from 'react-native';

import DataTable from 'components/DataTable';
import ChromaText from 'components/ChromaText';
import { CardView as PortfolioItemRow } from '../Transactions/TransactionsPresenter';

import './styles.styl';

export const Heading = React.memo((props = {}) => (
  <View styleName="Heading">
    <Text styleName="HeadingText">{props.text}</Text>
  </View>
));

type DataTableLabelPropTypes = {
  label: string,
};
function DataTableLabel(props: DataTableLabelPropTypes = {}) {
  return (
    <DataTable.Cell styleName="DataTableLabel">
      <Text styleName="DataTableLabelText">{props.label}</Text>
    </DataTable.Cell>
  );
}

type DataTableValuePropTypes = {
  as?: any,
  style?: StyleProp<ViewStyle>,
  value: string,
  currency?: string,
};
function DataTableValue(props: DataTableValuePropTypes = {}) {
  const TextComponent = props.as ? props.as : Text;

  return (
    <DataTable.Cell numeric={true} styleName="DataTableValue">
      <TextComponent styleName="DataTableValueText" value={props.value}>
        {Number.currency(props.value, '')}
      </TextComponent>
    </DataTable.Cell>
  );
}

//.연동 중인 계좌 정보
type LinkingAccountPropTypes = {
  security: 'ebest' | 'nh' | 'db' | 'yuanta',
  account_no: string,
};
export const LinkingAccount = React.memo(
  ({ security, account_no }: LinkingAccountPropTypes = {}) => {
    let readableSecurity = security;

    if (security === 'ebest') {
      readableSecurity = ' 이베스트투자증권';
    } else if (security === 'nh') {
      readableSecurity = 'NH투자증권';
    } else if (security === 'db') {
      readableSecurity = 'DB금융투자';
    } else if (security === 'yuanta') {
      readableSecurity === '유안타증권'
    }

    return (
      <View styleName="LinkingAccount">
        <Text styleName="LinkingAccountCaptionText">{readableSecurity}</Text>
        <Text styleName="LinkingAccountText">{account_no}</Text>
      </View>
    );
  },
);

// 연동 중인 계좌 잔고
type BalanceSummaryPropTypes = {
  total_assets_under_deposit: number, // 총 자산
  cash_amount_available: number, // 예수금 (현금보유)
  purchase_amount: number, // 매입금액
  valuation_amount: number, // 평가금액
  valuation_gain_loss: number, // 평가손익
  profit_and_loss_stack_sum: number, // 확정손익
};
export const BalanceSummary = React.memo(
  ({
    total_assets_under_deposit,
    cash_amount_available,
    purchase_amount,
    valuation_amount,
    valuation_gain_loss,
    profit_and_loss_stack_sum,
  }: BalanceSummaryPropTypes = {}) => {
    return (
      <DataTable styleName="ph15">
        <DataTable.Row>
          <DataTableLabel label="추정자산총액" />
          <DataTableLabel label="현금주문가능금액" />
        </DataTable.Row>
        <DataTable.Row styleName="underline">
          <DataTableValue value={total_assets_under_deposit} />
          <DataTableValue value={cash_amount_available} />
        </DataTable.Row>

        <DataTable.Row>
          <DataTableLabel label="매입금액" />
          <DataTableLabel label="평가금액" />
        </DataTable.Row>
        <DataTable.Row styleName="underline">
          <DataTableValue value={purchase_amount} />
          <DataTableValue value={valuation_amount} />
        </DataTable.Row>

        <DataTable.Row>
          <DataTableLabel label="평가손익" />
          <DataTableLabel label="확정손익" />
        </DataTable.Row>
        <DataTable.Row styleName="underline">
          <DataTableValue as={ChromaText} value={valuation_gain_loss} />
          <DataTableValue as={ChromaText} value={profit_and_loss_stack_sum} />
        </DataTable.Row>
      </DataTable>
    );
  },
);

//===
type PortfolioItemPropTypes = {
  stockcode: string,
  valuation_amount: number, // 평가금액
  balance_amount: number, // 보유수량
  valuation_gain_loss: number, // 평가손익
  purchase_amount: number, // 매입금액
  average_price: number, // 평균단가
  interest_rate: number, // 현수익률
  price: number, // 현재가
};

type PortfolioPropTypes = {
  portfolio: {
    items: [PortfolioItemPropTypes],
  },
} & $Rest<FlatListProps, {| data?: any, renderItem?: any |}>;

export function PortfolioList({
  portfolio,
  ...flatListProps
}: PortfolioPropTypes = {}) {
  const data = Array.isArray(portfolio?.items) ? portfolio.items : [];

  // 사용하지 않음
  delete flatListProps.data;

  return data.length > 0 ? (
    <FlatList
      data={data}
      keyExtractor={item => item.stockcode}
      renderItem={({ item }) => <PortfolioItemRow item={item} />}
      showsVerticalScrollIndicator={false}
      initialNumToRender={1}
      {...flatListProps}
    />
  ) : (
    <View styleName="TransactionEmptyList">
      <Text styleName="text14 regular caption">보유 중인 종목이 없습니다.</Text>
    </View>
  );
}
