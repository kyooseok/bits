/**
 *
 * @flow
 */
import React from 'react';
import { InteractionManager, ScrollView, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Navigation } from 'react-native-navigation';

import Settings from './Settings';

import 'styles/implement.styl';

export default class PreferencesScreen extends React.Component {
  static options() {
    return {
      topBar: {
        title: {
          text: '설정',
        },
      },
    };
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);
  }

  componentDidMount(): void {
    const { componentId, commandType } = this.props;

    if (commandType === 'showModal') {
      InteractionManager.runAfterInteractions(() => {
        MaterialCommunityIcons.getImageSource('close', 26, '#FFFFFF').then(
          icon => {
            Navigation.mergeOptions(componentId, {
              topBar: {
                rightButtons: [
                  {
                    id: 'DISMISS_MODAL',
                    icon: icon,
                  },
                ],
              },
            });
          },
        );
      });
    }
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'DISMISS_MODAL') {
      return Navigation.dismissModal(this.props.componentId);
    }
  }

  render() {
    return (
      <View styleName="compacted bg-background">
        <ScrollView>{React.createElement(Settings, this.props)}</ScrollView>
      </View>
    );
  }
}
