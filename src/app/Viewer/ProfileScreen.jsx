/**
 * 현재 사용자 정보조회
 *
 * @flow
 */
import React from 'react';
import {
  BackHandler,
  InteractionManager,
  Platform,
  ScrollView,
  View,
  ToastAndroid,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { BOTTOM_TABS_ID } from 'app/routeKeys';
import RouterService from 'services/RouterService';

import Balance from './Balance';
import Authenticator from './Authenticator';

import 'styles/implement.styl';

const MemoizedProfileScreen = React.memo((props = {}) => {
  return (
    <Authenticator>
      <ScrollView nestedScrollEnabled styleName="bg-background">
        <View styleName="pv13">{React.createElement(Balance, props)}</View>
      </ScrollView>
    </Authenticator>
  );
});

export default class ProfileScreen extends React.Component {
  static options(props = {}) {
    return Platform.select({
      ios: {
        topBar: {
          leftButtons: [
            {
              id: 'NAVIGATION_BUTTONS_BRAND',
              component: {
                name: 'TOP_BAR_BRAND',
              },
            },
          ],
        },
      },
      android: {
        topBar: {
          title: {
            component: {
              name: 'TOP_BAR_BRAND',
            },
          },
        },
      },
    });
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);

    this.state = {
      appeared: false,
    };
  }

  componentDidMount(): void {
    const { componentId } = this.props;
    InteractionManager.runAfterInteractions(() => {
      MaterialCommunityIcons.getImageSource('settings', 26, '#FFFFFF').then(
        icon => {
          Navigation.mergeOptions(componentId, {
            topBar: {
              rightButtons: [
                {
                  id: 'NAVIGATION_BUTTON_SETTINGS',
                  icon: icon,
                },
              ],
            },
          });
        },
      );
    });
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'NAVIGATION_BUTTON_SETTINGS') {
      return RouterService.showViewerSettingsModal();
    }
  }

  componentDidAppear() {
    this.setState({ appeared: true });

    BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPressed);
  }

  componentDidDisappear() {
    this.setState({ appeared: false });

    this.hardwareBackPressedTime = null;

    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.hardwareBackPressed,
    );
  }

  hardwareBackPressed() {
    const timestamp = new Date().getTime();
    if (
      this.hardwareBackPressedTime &&
      this.hardwareBackPressedTime + 3000 > timestamp
    ) {
      return false;
    }

    this.hardwareBackPressedTime = timestamp;
    ToastAndroid.show('한번 더 누르면 앱이 종료됩니다.', ToastAndroid.SHORT);
    return true;
  }

  render() {
    const props = {
      ...this.props,
      appeared: this.state.appeared,
    };

    return React.createElement(MemoizedProfileScreen, props);
  }
}
