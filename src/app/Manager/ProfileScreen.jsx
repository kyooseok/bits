// @flow
import React from 'react';
import { InteractionManager, Platform, StyleSheet, View } from 'react-native';
import { Navigation } from 'react-native-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { autobind } from 'core-decorators';
import SubmitButton from 'components/SubmitButton';
import WebView from 'components/WebView';

import RouterService from 'services/RouterService';

import 'styles/implement.styl';

@autobind
export default class ProfileScreen extends React.Component {
  productName: string;

  static options() {
    return Platform.select({
      ios: {
        topBar: {
          leftButtons: [
            {
              id: 'NAVIGATION_BUTTONS_BRAND',
              component: {
                name: 'TOP_BAR_BRAND',
              },
            },
          ],
        },
      },
      android: {
        topBar: {
          title: {
            component: {
              name: 'TOP_BAR_BRAND',
            },
          },
        },
      },
    });
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);
  }

  componentDidMount(): void {
    const { componentId, commandType } = this.props;

    if (commandType === 'showModal') {
      InteractionManager.runAfterInteractions(() => {
        MaterialCommunityIcons.getImageSource('close', 26, '#FFFFFF').then(
          icon => {
            Navigation.mergeOptions(componentId, {
              topBar: {
                rightButtons: [
                  {
                    id: 'DISMISS_MODAL',
                    icon: icon,
                  },
                ],
              },
            });
          },
        );
      });
    }
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'DISMISS_MODAL') {
      return Navigation.dismissModal(this.props.componentId);
    }
  }

  onNavigationStateChange(e) {
    // 로딩이 완료 된 후 해당 상품명을 가져옴
    if (e.loading === false) {
      this.productName = e.title;
    }
  }

  handleSubmit() {
    return RouterService.showContactsModal({ category: this.productName });
  }

  render() {
    const { uri, componentId } = this.props;

    return (
      <View styleName="compacted bg-background">
        <WebView
          source={{ uri }}
          componentId={componentId}
          onNavigationStateChange={this.onNavigationStateChange}
        />

        <View
          style={[StyleSheet.absoluteFillObject, { top: 'auto', padding: 15 }]}>
          <SubmitButton
            title="상담 신청하기"
            // submitting={formState.isSubmitting}
            handleSubmit={this.handleSubmit}
            // buttonProps={{disabled: !formState.isValid}}
          />
        </View>
      </View>
    );
  }
}
