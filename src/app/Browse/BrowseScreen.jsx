// @flow
import React from 'react';
import { BackHandler, Platform, ToastAndroid, View } from 'react-native';
import { Navigation } from 'react-native-navigation';
import WebView from 'react-native-webview';

import { autobind } from 'core-decorators';

import {
  ASP_COPY_TRADING_SYSTEM_PRODUCT_SCREEN,
  WEB_VIEW_SCREEN,
} from '../routeKeys';

import RouterService from '../../services/RouterService';

import 'styles/implement.styl';

const ROOT_WEB_URL = 'https://thehantrader.com/app/analysis_detail';

@autobind
export default class BrowseScreen extends React.Component {
  productName: string;

  static options(props = {}) {
    return Platform.select({
      ios: {
        topBar: {
          leftButtons: [
            {
              id: 'NAVIGATION_BUTTONS_BRAND',
              component: {
                name: 'TOP_BAR_BRAND',
              },
            },
          ],
        },
      },
      android: {
        topBar: {
          title: {
            component: {
              name: 'TOP_BAR_BRAND',
            },
          },
        },
      },
    });
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);
  }


  componentDidAppear() {
    this.setState({ appeared: true });

    BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPressed);
  }

  componentDidDisappear() {
    this.setState({ appeared: false });

    this.hardwareBackPressedTime = null;

    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.hardwareBackPressed,
    );
  }

  hardwareBackPressed() {
    const timestamp = new Date().getTime();
    if (
      this.hardwareBackPressedTime &&
      this.hardwareBackPressedTime + 3000 > timestamp
    ) {
      return false;
    }

    this.hardwareBackPressedTime = timestamp;
    ToastAndroid.show('한번 더 누르면 앱이 종료됩니다.', ToastAndroid.SHORT);
    return true;
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'DISMISS_MODAL') {
      return Navigation.dismissModal(this.props.componentId);
    }
  }

  _onNavigationStateChange(e) {
    // 로딩이 완료 된 후 해당 상품명을 가져옴
    if (e.loading === false) {
      this.productName = e.title;
    }
  }

  _onShouldStartLoadWithRequest(request) {
    const showNextFrameFn =
      request.url.indexOf('/cts_tab/cts_detail') !== -1
        ? RouterService.showManagerModal
        : RouterService.showWebViewModal;

    // const { host, pathname } = urlParse(request.url);

    if (ROOT_WEB_URL === request.url) {
      return true;
    }

    setImmediate(() => {
      return showNextFrameFn({
        uri: request.url,
      });
    });

    return false;
  }

  render() {
    return (
      <View styleName="compacted bg-background">
        <WebView
          allowsFullscreenVideo
          allowsInlineMediaPlayback
          source={{ uri: ROOT_WEB_URL }}
          originWhitelist={['*']}
          onNavigationStateChange={this._onNavigationStateChange}
          onShouldStartLoadWithRequest={this._onShouldStartLoadWithRequest}
        />
      </View>
    );
  }
}
