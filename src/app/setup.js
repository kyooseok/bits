'use strict';

import { Image } from 'react-native';
import { Navigation } from 'react-native-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import Guide from 'components/Guide/Guide';
import AppCache, { ASK_ACCESS_TOKEN } from './cache';
import navigationOptions from './navigationOptions';
// import useObservable from 'utils/hooks/useObservable';
// import { useObserver } from 'mobx-react';

import {
  BROWSE_SCREEN,
  FOREIGN_SCREEN,
  BOTTOM_TABS_ID,
  // ROUTE_MAP_SCREEN,
  VIEWER_PROFILE_SCREEN,
  LOGIN_SCREEN,
} from './routeKeys';

export function setup(isLogined) {
  return navigationOptions()
  .then(options => Navigation.setDefaultOptions(options))
  .then(() => {
    let isLogined = AppCache.get(ASK_ACCESS_TOKEN);
    return isLogined;
  })
  .then((isLogined)=> {
    if(!isLogined) {
      setLoginLayout();
    }else{
      setBottomTabsBasedLayout();
    }
  });
}



/**
 *
 * @returns {Promise<unknown>|Promise<any>}
 */

function setLoginLayout() {
  return Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: LOGIN_SCREEN,
            },
          },
        ],
      },
    },
  });
}

function setBottomTabsBasedLayout() {
  const promises = [
    MaterialCommunityIcons.getImageSource('home-variant', 28),
    MaterialCommunityIcons.getImageSource('clipboard-flow', 32),
    MaterialCommunityIcons.getImageSource('android-messages', 32),
  ];

  return Promise.all(promises).then(icons => {
    Image.getSize(icons[0].uri, (width, height) =>
      console.log('icon size', width, height),
    );

    return Navigation.setRoot({
      root: {
        bottomTabs: {
          id: BOTTOM_TABS_ID,
          children: [
            {
              stack: {
                children: [
                  {
                    component: {
                      name: VIEWER_PROFILE_SCREEN,
                    },
                  },
                ],
                options: {
                  bottomTab: {
                    text: '홈',
                    icon: icons[0],
                    selectedIcon: icons[0],
                  },
                },
              },
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: FOREIGN_SCREEN,
                    },
                  },
                ],
                options: {
                  bottomTab: {
                    text: '외국계',
                    icon: icons[1],
                    selectedIcon: icons[1],
                  },
                },
              },
            },
            // Tab1
            {
              stack: {
                children: [
                  {
                    component: {
                      name: BROWSE_SCREEN,
                    },
                  },
                ],
                options: {
                  bottomTab: {
                    text: '리포트',
                    icon: icons[2],
                    selectedIcon: icons[2],
                  },
                },
              },
            },
            // Tab2
          ],
        },
      },
    });
  });
}
