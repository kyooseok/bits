import React from 'react';
import { Image } from 'react-native';

export default function TopBarBrand() {
  return (
    <Image
      source={require('assets/images/ci_bits.png')}
      resizeMode="contain"
      style={{ width: 60, height: 19 }}
    />
  );
}
