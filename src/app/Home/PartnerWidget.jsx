// @flow
import React from 'react';
import { StyleSheet, FlatList, Image, View, Text } from 'react-native';
import { useQuery } from 'react-query';

import partnerMock from './__mocks__/partners';
import RestClientService from 'services/RestClientService';

import './styles.styl';

type PartnerItemPropTypes = {
  name: string, // 회사명
  image: string, // 회사이미지
  description: string, // 회사소개
  운영상품수: number,
  tags: [string],
};

const ItemRow = React.memo(({ item } = {}) => {
  const imageSource = String(item.image).replace(
    '../../',
    'https://thehantrader.com/',
  );

  return (
    <View
      style={[
        styles.shadow,
        {
          width: 165,
          borderRadius: 6,
          backgroundColor: 'white',
          margin: 5,
        },
      ]}>
      <View style={{ alignItems: 'center', paddingTop: 15 }}>
        <Image
          source={{ uri: imageSource }}
          style={{ width: 64, height: 64 }}
        />
        <Text styleName="text14 medium">{item.name}</Text>
        <Text styleName="text13 regular caption">{item.port}개 상품 운용 중</Text>
      </View>

      <View styleName="p15">
        <View styleName="row">
          <View styleName="Badge bg-danger">
            <Text styleName="BadgeText">PRIVATE</Text>
          </View>
          <View styleName="Badge bg-success">
            <Text styleName="BadgeText">PUBLIC</Text>
          </View>
        </View>
      </View>
    </View>
  );
});

function queryFn(): Promise<[PartnerItemPropTypes]> {
  // return RestClientService.get('http://sa.wlfa.us/recent-naver-post.json')
  //   .then(response => response.body)
  //   .then(({ data }) => data.items);

  return Promise.resolve(partnerMock).then(({ data }) => data.items);
}

export default function PartnerWidget(props = {}) {
  const { status, data } = useQuery('PartnerWidget', queryFn);

  const itemRenderFn = ({ item }) => <ItemRow item={item} />;

  return (
    status === 'success' && (
      <FlatList
        data={data}
        keyExtractor={item => item.name}
        renderItem={itemRenderFn}
        initialNumToRender={3}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        style={{ paddingHorizontal: 5 }}
      />
    )
  );
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  tagItem: {
    borderWidth: 1,
    borderColor: '#eeeeee',
    padding: 6,
    paddingVertical: 3,
    marginRight: 3,
  },
});
