// @flow
import React from 'react';
import { ScrollView, View, Text } from 'react-native';

import PartnerWidget from './PartnerWidget';
import RecentNPostWidget from './RecentNPostWidget';

import './styles.styl';
import { Touchable } from 'components/Button';
import RouterService from 'services/RouterService';
import ResponsiveImage from 'components/Image/Responsive';

export default function Home(props = {}) {
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={{ paddingLeft: 15, paddingTop: 25, paddingBottom: 5 }}>
        <Text styleName="text19 medium">파트너사</Text>
      </View>
      <PartnerWidget />

      <View style={{ paddingTop: 21, paddingBottom: 6 }}>
        <Touchable
          activeOpacity={0.9}
          onPress={() =>
            RouterService.showWebViewModal({
              uri:
                'https://cs.wlfa.us/kb/%EB%8D%94%ED%95%9C%ED%8A%B8%EB%A0%88%EC%9D%B4%EB%8D%94cts-%EC%86%8C%EA%B0%9C',
            })
          }>
          <ResponsiveImage
            source={{
              uri: 'https://thehantrader.com/images/cts_tab/banner_cts2.png',
            }}
          />
        </Touchable>
      </View>

      <View style={{ paddingLeft: 15, paddingTop: 25, paddingBottom: 5 }}>
        <Text styleName="text19 medium">포스트</Text>
      </View>
      <RecentNPostWidget />

      <View style={{ height: 25 }} />
    </ScrollView>
  );
}
