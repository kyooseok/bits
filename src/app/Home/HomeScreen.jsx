// @flow
import React from 'react';
import {
  Platform,
  BackHandler,
  ToastAndroid,
  View,
  InteractionManager,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import RouterService from 'services/RouterService';
import Home from './Home';

import 'styles/implement.styl';

export default class HomeScreen extends React.PureComponent {
  static options(props = {}) {
    return Platform.select({
      ios: {
        topBar: {
          leftButtons: [
            {
              id: 'NAVIGATION_BUTTONS_BRAND',
              component: {
                name: 'TOP_BAR_BRAND',
              },
            },
          ],
        },
      },
      android: {
        topBar: {
          title: {
            component: {
              name: 'TOP_BAR_BRAND',
            },
          },
        },
      },
    });
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);

    this.state = {
      appeared: true,
    };
  }

  componentDidMount(): void {
    const { componentId } = this.props;
    InteractionManager.runAfterInteractions(() => {
      MaterialCommunityIcons.getImageSource('settings', 26, '#FFFFFF').then(
        icon => {
          Navigation.mergeOptions(componentId, {
            topBar: {
              rightButtons: [
                {
                  id: 'NAVIGATION_BUTTON_SETTINGS',
                  icon: icon,
                },
              ],
            },
          });
        },
      );
    });
  }

  componentDidAppear() {
    this.setState({ appeared: true });

    BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPressed);
  }

  componentDidDisappear() {
    this.setState({ appeared: false });

    this.hardwareBackPressedTime = null;

    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.hardwareBackPressed,
    );
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'NAVIGATION_BUTTON_SETTINGS') {
      return RouterService.showViewerSettingsModal();
    }
  }

  hardwareBackPressed() {
    const timestamp = new Date().getTime();
    if (
      this.hardwareBackPressedTime &&
      this.hardwareBackPressedTime + 3000 > timestamp
    ) {
      return false;
    }

    this.hardwareBackPressedTime = timestamp;
    ToastAndroid.show('한번 더 누르면 앱이 종료됩니다.', ToastAndroid.SHORT);
    return true;
  }

  render() {
    const props = {
      ...this.props,
      appeared: this.state.appeared,
    };

    return (
      <View styleName="compacted bg-background">
        {React.createElement(Home, props)}
      </View>
    );
  }
}
