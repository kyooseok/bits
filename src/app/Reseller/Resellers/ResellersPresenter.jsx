// @flow
import React from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  StyleProp,
  ViewStyle,
} from 'react-native';

import { Touchable } from 'components/Button';
import ResponsiveImage from 'components/Image/Responsive';
import stubArray from 'utils/stubArray';

import './styles.styl';

export type PartnerItemPropTypes = {
  name: string, // 회사명
  logoURL: string, // 회사이미지
  resellerDescription?: string, // 회사소개
  productCount: number,
  tagList: [string],
  detailURL: string,
};

export type PartnersItemRowPropTypes = {
  item: PartnerItemPropTypes,
  height: number,
  index?: number,
  style?: StyleProp<ViewStyle>,
  onPress: (item: PartnerItemPropTypes) => void,
};

function ItemRowComponent({
  item,
  height,
  onPress,
  style,
}: PartnersItemRowPropTypes) {
  const imageSource = String(item.logoURL).replace(
    '../../',
    'https://thehantrader.com/',
  );

  const containerStyle = StyleSheet.flatten([
    { height: parseInt(height, 10) || 84 },
    style,
  ]);

  return (
    <Touchable styleName="ItemRow" style={containerStyle} onPress={onPress}>
      <Image
        source={{ uri: imageSource }}
        styleName="ItemRowImage mr15"
        resizeMode="contain"
      />

      <View styleName="compacted">
        <Text styleName="text15 medium" numberOfLines={2}>
          {item.name}
        </Text>
        <Text styleName="text13 regular caption">
          {item.productCount}개 상품 운용 중
        </Text>
      </View>

      <View styleName="row">
        {stubArray(item.tagList).map(badge => (
          <ItemRowBadge badge={badge} />
        ))}
      </View>
    </Touchable>
  );
}

export const ItemRow = React.memo(ItemRowComponent);

type ItemRowBadgePropTypes = {
  badge: string,
};
function ItemRowBadge({ badge }: ItemRowBadgePropTypes = {}) {
  let color,
    badgeText = String(badge).toUpperCase();

  switch (badgeText) {
    case 'PRIVATE':
      color = '#DC3545';
      break;
    case 'PUBLIC':
      color = '#28A745';
      break;
    default:
      color = '#ccc8c9';
      break;
  }

  return (
    <View styleName="ItemRowBadge" style={{ backgroundColor: color }}>
      <Text styleName="ItemRowBadgeText">{badgeText}</Text>
    </View>
  );
}

export const ItemRowSeparator = React.memo(() => (
  <View styleName="ItemRowSeparator" />
));

export const ListHeaderComponent = React.memo(() => (
  <View styleName="pv6">
    <ResponsiveImage
      source={{
        uri: 'https://cdn.wlfa.us/banners/thehan_reseller_banner_20200417.png',
      }}
    />
  </View>
));
