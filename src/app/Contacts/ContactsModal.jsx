/**
 * @author  Martin, Lee
 * @since   2020.03.29
 *
 * @flow
 */
import React from 'react';
import { Platform, View } from 'react-native';
import { Navigation } from 'react-native-navigation';

import ContactsForm from './Form';
import './styles.styl';

export default class ContactsModal extends React.PureComponent {
  static options(props = {}) {
    let navigationOptions = Platform.select({
      ios: {
        topBar: {
          leftButtons: [
            {
              id: 'NAVIGATION_BUTTONS_BRAND',
              component: {
                name: 'TOP_BAR_BRAND',
              },
            },
          ],
        },
      },
      android: {
        topBar: {
          title: {
            component: {
              name: 'TOP_BAR_BRAND',
            },
          },
        },
      },
    });

    navigationOptions.sideMenu = {
      left: { enabled: false },
    };

    if (props.commandType === 'showModal') {
      navigationOptions.topBar.rightButtons = [
        {
          id: 'DISMISS_MODAL',
          icon: require('assets/images/icons/ic_close.png'),
        },
      ];
    }

    return navigationOptions;
  }

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'DISMISS_MODAL') {
      return Navigation.dismissModal(this.props.componentId);
    }
  }

  render() {
    return (
      <View styleName="compacted bg-background">
        {React.createElement(ContactsForm, this.props)}
      </View>
    );
  }
}
