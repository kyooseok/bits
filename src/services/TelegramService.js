const BOT_TOKEN = '879034252:AAH0OXV8XBwfTbNR4Daf_HOEvh_p0pYVP_U';
const TELEGRAM_BOT_URL = `https://api.telegram.org/bot${BOT_TOKEN}`;

class TelegramService {
  sendAsync(params = {}) {
    console.log(params, TELEGRAM_BOT_URL + '/sendMessage');

    return fetch(TELEGRAM_BOT_URL + '/sendMessage', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    }).then(r => r.json());
  }
}

function toEscapeMessage(message) {
  return String(message)
    .replace(/\./g, '\\.')
    .replace(/_/g, '\\_')
    .replace(/\[/g, '\\[')
    .replace(/\(/g, '\\(')
    .replace(/\)/g, '\\)')
    .replace(/`/g, '\\`')
    .replace(/\*/g, '\\*');
}

export default new TelegramService();
