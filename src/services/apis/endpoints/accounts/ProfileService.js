import { get, isNil } from 'lodash';
import { autobind } from 'core-decorators';
import RestClientService from 'services/RestClientService';

@autobind
export class ProfileService {
  getAsync(options) {
    return RestClientService.get('/api/user/detail', options)
      .then(response => get(response.body, 'data'))
      .then(response => {
        if (isNil(response) || isNil(response?.name)) {
          throw { code: 403, message: '접근 권한이 없습니다.' };
        }
        return response;
      });
  }
}

export default new ProfileService();
