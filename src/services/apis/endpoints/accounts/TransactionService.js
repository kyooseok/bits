import { get } from 'lodash';
import { autobind } from 'core-decorators';
import RestClientService from 'services/RestClientService';

@autobind
export class TransactionService {
  queryAsync(options) {
    return RestClientService.get('/api/user/hts_my_grid_log', options).then(
      response => {
        return get(response.body, 'data');
        // return response.body;
      },
    );
  }
}

export default new TransactionService();
