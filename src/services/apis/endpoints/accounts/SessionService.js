import { autobind } from 'core-decorators';
import RestClientService from 'services/RestClientService';

@autobind
export class SessionService {
  createAsync(options) {
    return RestClientService.post('/api/thehanapp/user/login', options)
      .then(response => response.body)
      .then(response => {
        if (response.code && response.msg) {
          throw { code: response.code, message: response.msg };
        }

        return response;
      });
  }

  pingAsync(options) {
    return RestClientService.get('/api/thehanapp/user/token', options).then(
      response => response.body,
    );
  }
}

export default new SessionService();
