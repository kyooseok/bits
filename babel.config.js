module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    'babel-plugin-styled-components',
    'transform-inline-environment-variables',
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    [
      'react-native-stylename-to-style',
      {
        extensions: ['css', 'styl'],
      },
    ],
  ],
  env: {
    production: {
      plugins: ['transform-remove-console'],
    },
  },
};
